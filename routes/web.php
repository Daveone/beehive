<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*
 *      Registration
 */
Auth::routes(
    ['register' => false,
        'verify' => true]
);

Route::resource('users', 'UsersProfile');


Route::get('/profile', 'UsersProfile@show')->name('profile');
Route::post('/profile', 'UsersProfile@store')->name('profile');


Route::get('/home', 'HomeController@index')->name('home');

