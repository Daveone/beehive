<?php
return [
    "A fresh verification link has been sent to your email address." => "A fresh verification link has been sent to your email address.",
    "Before proceeding, please check your email for a verification link." => "Before proceeding, please check your email for a verification link.",
    "Confirm Password" => "Confirm Password",
    "Don't have an account?" => "Don't have an account?",
    "E-Mail Address" => "E-Mail Address",
    "Forgot Your Password?" => "Forgot Your Password?",
    "If you did not receive the email" => "If you did not receive the email",
    "Login" => "Login",
    "Logout" => "Abmelden",
    "Name" => "Name",
    "Password" => "Password",
    "Please confirm your password before continuing." => "Please confirm your password before continuing.",
    "Register" => "Register",
    "Remember Me" => "Remember Me",
    "Reset Password" => "Reset Password",
    "Verify Your Email Address" => "Verify Your Email Address",
    "You are logged in!" => "You are logged in!",
    "failed" => "These credentials do not match our records.",
    "reset" => "Your password has been reset!",
    "sent" => "We have emailed your password reset link!",
    "throttle" => "Too many login attempts. Please try again in :seconds seconds.",
    "throttled" => "Please wait before retrying.",
    "token" => "This password reset token is invalid.",
    "user" => "We can't find a user with that email address."
];
