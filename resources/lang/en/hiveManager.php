<?php
return [
    "Bee diseases" => "Bee diseases",
    "HiveManager" => "HiveManager",
    "Knowledge base" => "Knowledge base",
    "My Account" => "My Account",
    "Notes" => "Notes",
    "Notifications" => "Notifications",
    "Search" => "Search",
    "Trade beekeeping" => "Trade beekeeping"
];
