<?php
return [
    "Bee diseases" => "Bienenkrankheiten",
    "HiveManager" => "Beutenverwaltung",
    "Knowledge base" => "Wissensdatenbank",
    "My Account" => "Mein Konto",
    "Notes" => "Anmerkungen",
    "Notifications" => "Benachrichtigungen",
    "Search" => "Suche",
    "Trade beekeeping" => "Bienenbörse"
];
