@extends('layouts.app')

@section('content')


    <h1>Profil bearbeiten</h1>


    <div class="bg-green-100 border-l-4 border-green-500 text-black-700 p-4" role="alert">
        <p class="font-bold">Information</p>
        <p>
            Warum benötigen wir Ihre Adresse? -
            Garnicht!<br>
            <b>PLZ und Adresse </b> -> Übersicht für andere "Wer hat was gemacht hat"<br>
            <b>Firma / URL</b> Anzeige in Ihrem Profil "Werbung"

            <br><br>
            <small>Sie sind nicht zur Eingabe verpflichtet!</small>

        </p>
    </div>



    <form class="w-full max-w-lg" action="/profile" method="post">
        @csrf
        <div class="flex flex-wrap -mx-3 mb-2">
            <div class="w-full md:w-2/3 px-3 mb-6 md:mb-0">
                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-adress">
                    Street
                </label>
                <input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded
            py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500 @error('adress') border-red-500 @enderror"
                       id="grid-adress" type="text" name="adress" placeholder="Street" value="{{old('adress') ?? $userData->adress ?? ''}}">
                @error('adress')<p class="form-error-msg">{{ $errors->first('adress') }}</p>@enderror
            </div>

            <div class="w-full md:w-1/3 px-3 mb-6 md:mb-0">
                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-number">
                    Number
                </label>
                <input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded
            py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500 @error('adress_number') border-red-500 @enderror"
                       id="grid-adress_number" type="text" name="adress_number" placeholder="Number" value="{{old('adress_number') ?? $userData->adress_number ?? ''}}">
                @error('adress_number')<p class="form-error-msg">{{ $errors->first('adress_number') }}</p>@enderror
            </div>

            <div class="w-full md:w-1/3 px-3 mb-6 md:mb-0">
                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-zip">
                    Zip
                </label>
                <input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4
            leading-tight focus:outline-none focus:bg-white focus:border-gray-500 @error('zip') border-red-500 @enderror"
                       id="grid-zip" type="text" name="zip" placeholder="90210" value="{{old('zip') ?? $userData->zip ?? ''}}">
                @error('zip')<p class="form-error-msg">{{ $errors->first('zip') }}</p>@enderror
            </div>

        </div>
        <div class="flex flex-wrap -mx-3 mb-2">

            <div class="w-full md:w-1/3 px-3 mb-6 md:mb-0">
                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2 " for="grid-phone">
                    Phone
                </label>
                <input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded
            py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500 @error('phone') border-red-500 @enderror"
                       id="grid-phone" type="text" name="phone" placeholder="Phone" value="{{old('site') ?? $userData->phone ?? ''}}">
                @error('phone')<p class="form-error-msg">{{ $errors->first('phone') }}</p>@enderror
            </div>

            <div class="w-full md:w-1/3 px-3 mb-6 md:mb-0">
                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-mobile">
                    Mobile
                </label>
                <input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4
            leading-tight focus:outline-none focus:bg-white focus:border-gray-500 @error('mobile') border-red-500 @enderror"
                       id="grid-mobile" type="text" name="mobile" placeholder="Mobile" value="{{old('mobile') ?? $userData->mobile ?? ''}}">
                @error('mobile')<p class="form-error-msg">{{ $errors->first('mobile') }}</p>@enderror
            </div>

        </div>

        <div class="flex flex-wrap -mx-3 mb-2">

            <div class="w-full md:w-2/3 px-3 mb-6 md:mb-0">
                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-company_name">
                    Company Name
                </label>
                <input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded
            py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500 @error('company_name') border-red-500 @enderror"
                       id="grid-company_name" type="text" name="company_name" placeholder="Company" value="{{old('company_name') ?? $userData->company_name ?? ''}}">
                @error('company_name')<p class="form-error-msg">{{ $errors->first('company_name') }}</p>@enderror
            </div>

            <div class="w-full md:w-2/3 px-3 mb-6 md:mb-0">
                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-company_name">
                    Webseite
                </label>
                <input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded
            py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500 @error('site') border-red-500 @enderror"
                       id="grid-company_name" type="text" name="site" placeholder="Company" value="{{old('site') ?? $userData->site ?? ''}}">
                @error('site')<p class="form-error-msg">{{ $errors->first('site') }}</p>@enderror
            </div>


        </div>

        <button type="submit" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">Profil speichern</button>


    </form>

@endsection
