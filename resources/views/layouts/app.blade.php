<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'BeeHive') }}</title>

    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
</head>
<body class="bg-gray-100 font-sans leading-normal tracking-normal">

<nav id="header" class="bg-white fixed w-full z-10 top-0 shadow">

    <diV class="absolute bottom-4 left-150 pl-8 ">
        <a class="text-gray-900 text-base xl:text-xl no-underline hover:no-underline font-bold " href="">
            <img class=" object-center w-24 h-24 pb-2 pt-1" src="/image/logooText.png" width="50px" height="50px">
        </a>
    </diV>

    <div class="w-full container mx-auto flex flex-wrap items-center mt-0 pt-3 pb-3 md:pb-0">

        <div class="w-1/2 pl-2 md:pl-0">
        </div>
        <div class="w-1/2 pr-0">
            <div class="flex relative inline-block float-right">

                <div class="relative text-sm">
                    @guest
                    @else
                    <button id="userButton" class="flex items-center focus:outline-none mr-3">
                        <img class="w-8 h-8 rounded-full mr-4" src="/image/300.jpg" alt="Avatar of User">
                        <span class="hidden md:inline-block">
                            Hi, {{ Auth::user()->name }}
                        </span>
                        <svg class="pl-2 h-2" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 129 129"
                             xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 129 129">
                            <g>
                                <path
                                    d="m121.3,34.6c-1.6-1.6-4.2-1.6-5.8,0l-51,51.1-51.1-51.1c-1.6-1.6-4.2-1.6-5.8,0-1.6,1.6-1.6,4.2 0,5.8l53.9,53.9c0.8,0.8 1.8,1.2 2.9,1.2 1,0 2.1-0.4 2.9-1.2l53.9-53.9c1.7-1.6 1.7-4.2 0.1-5.8z"/>
                            </g>
                        </svg>
                    </button>
                    @endguest


                    <div id="userMenu"
                         class="bg-white rounded shadow-md mt-2 absolute mt-12 top-0 right-0 min-w-full overflow-auto z-30 invisible">
                        <ul class="list-reset">
                            @guest
                            <a class="no-underline hover:underline text-gray-300 text-sm p-3"
                               href="{{ route('login') }}">{{ __('Login') }}</a>
                            @if (Route::has('register'))
                            <a class="no-underline hover:underline text-gray-300 text-sm p-3"
                               href="{{ route('register') }}">{{ __('Register') }}</a>
                            @endif
                            @else
                            <li><a href="{{route('profile')}}"
                                   class="px-4 py-2 block text-gray-900 hover:bg-gray-400 no-underline hover:no-underline">{{ __('hiveManager.My Account') }}</a></li>
                            <li><a href="#"
                                   class="px-4 py-2 block text-gray-900 hover:bg-gray-400 no-underline hover:no-underline cursor-not-allowed">
                                    {{ __('hiveManager.Notifications') }}</a>
                            </li>
                            <li>
                                <hr class="border-t mx-2 border-gray-400">
                            </li>
                            <li><a href="{{ route('logout') }}"
                                   class="px-4 py-2 block text-gray-900 hover:bg-gray-400 no-underline hover:no-underline"
                                   onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">{{ __('auth.Logout') }}</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="hidden">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                            @endguest
                        </ul>
                    </div>
                </div>

            </div>

        </div>
        @guest
        @else
        @include('layouts.nav')
        @endguest
    </div>


</nav>




<div class="container w-full mx-auto pt-20">

    <div class="w-full px-4 md:px-0 md:mt-8 mb-16 text-grey-darkest leading-normal">


        @yield('content')

    </div>
</div>



<footer class="bg-white border-t border-gray-400 shadow">
    @include('layouts.footer')
</footer>

<script>
    /*Toggle dropdown list*/
    /*https://gist.github.com/slavapas/593e8e50cf4cc16ac972afcbad4f70c8*/

    var userMenuDiv = document.getElementById("userMenu");
    var userMenu = document.getElementById("userButton");

    var navMenuDiv = document.getElementById("nav-content");
    var navMenu = document.getElementById("nav-toggle");

    document.onclick = check;

    function check(e) {
        var target = (e && e.target) || (event && event.srcElement);

        //User Menu
        if (!checkParent(target, userMenuDiv)) {
            // click NOT on the menu
            if (checkParent(target, userMenu)) {
                // click on the link
                if (userMenuDiv.classList.contains("invisible")) {
                    userMenuDiv.classList.remove("invisible");
                } else {
                    userMenuDiv.classList.add("invisible");
                }
            } else {
                // click both outside link and outside menu, hide menu
                userMenuDiv.classList.add("invisible");
            }
        }

        //Nav Menu
        if (!checkParent(target, navMenuDiv)) {
            // click NOT on the menu
            if (checkParent(target, navMenu)) {
                // click on the link
                if (navMenuDiv.classList.contains("hidden")) {
                    navMenuDiv.classList.remove("hidden");
                } else {
                    navMenuDiv.classList.add("hidden");
                }
            } else {
                // click both outside link and outside menu, hide menu
                navMenuDiv.classList.add("hidden");
            }
        }

    }

    function checkParent(t, elm) {
        while (t.parentNode) {
            if (t == elm) {
                return true;
            }
            t = t.parentNode;
        }
        return false;
    }
</script>

</body>

</html>
