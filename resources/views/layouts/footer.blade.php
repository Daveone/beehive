
@guest
<div class="container max-w-md mx-auto flex py-8">

    <div class="w-full mx-auto flex flex-wrap">
        <div class="flex w-full md:w-1/2 ">
            <div class="px-8">
                <h3 class="font-bold font-bold text-gray-900">Vorteile und Funktionen</h3>
                <ul class="list-reset items-center text-sm pt-3">
                    <li>
                        <a class="inline-block text-gray-600 no-underline hover:text-gray-900 hover:text-underline py-1"
                           href="#">Add social link</a>
                    </li>
                    <li>
                        <a class="inline-block text-gray-600 no-underline hover:text-gray-900 hover:text-underline py-1"
                           href="#">Add social link</a>
                    </li>
                    <li>
                        <a class="inline-block text-gray-600 no-underline hover:text-gray-900 hover:text-underline py-1"
                           href="#">Add social link</a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="flex w-full md:w-1/2">
            <div class="px-8">
                <h3 class="font-bold font-bold text-gray-900">Social</h3>
                <ul class="list-reset items-center text-sm pt-3">
                    <li>
                        <a class="inline-block text-gray-600 no-underline hover:text-gray-900 hover:text-underline py-1"
                           href="#">Bienenkönigen</a>
                    </li>
                    <li>
                        <a class="inline-block text-gray-600 no-underline hover:text-gray-900 hover:text-underline py-1"
                           href="#">Wabenhygiene</a>
                    </li>
                    <li>
                        <a class="inline-block text-gray-600 no-underline hover:text-gray-900 hover:text-underline py-1"
                           href="#">Honig</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>


</div>

@else
<div class="container mx-auto flex py-6">

    <div class="w-full mx-auto flex flex-wrap">

        <div class="flex w-full md:w-1/3 ">
            <div class="px-2">
                <h3 class="font-bold font-bold text-gray-900">Wissensdatenbank</h3>
                <ul class="list-reset items-center text-sm pt-3">
                    <li>
                        <a class="inline-block text-gray-600 no-underline hover:text-gray-900 hover:text-underline py-1 cursor-not-allowed"
                           href="#">Meine Beiträge</a>
                    </li>
                    <li>
                        <a class="inline-block text-gray-600 no-underline hover:text-gray-900 hover:text-underline py-1 cursor-not-allowed"
                           href="#">Beitrag erstellen</a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="flex w-full md:w-1/3">
            <div class="px-2">
                <h3 class="font-bold font-bold text-gray-900">Neue Beiträge</h3>
                <ul class="list-reset items-center text-sm pt-3">
                    <li>
                        <a class="inline-block text-gray-600 no-underline hover:text-gray-900 hover:text-underline py-1 cursor-not-allowed"
                           href="#">Bienenkönigen</a>
                    </li>
                    <li>
                        <a class="inline-block text-gray-600 no-underline hover:text-gray-900 hover:text-underline py-1 cursor-not-allowed"
                           href="#">Wabenhygiene</a>
                    </li>
                    <li>
                        <a class="inline-block text-gray-600 no-underline hover:text-gray-900 hover:text-underline py-1 cursor-not-allowed"
                           href="#">Honig</a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="flex w-full md:w-1/3">
            <div class="px-2">
                <h3 class="font-bold font-bold text-gray-900">Projektinformationen</h3>
                <ul class="list-reset items-center text-sm pt-3">
                    <li>
                        <a class="inline-block text-gray-600 no-underline hover:text-gray-900 hover:text-underline py-1 cursor-not-allowed"
                           href="#">Das Projekt</a>
                    </li>
                    <li>
                        <a class="inline-block text-gray-600 no-underline hover:text-gray-900 hover:text-underline py-1 cursor-not-allowed"
                           href="#">Änderungen</a>
                    </li>
                    <li>
                        <a class="inline-block text-gray-600 no-underline hover:text-gray-900 hover:text-underline py-1 cursor-not-allowed"
                           href="#">0,00 €</a>
                    </li>
                </ul>
            </div>
        </div>



    </div>


</div>
@endguest
