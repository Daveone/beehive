<?php

namespace App\Http\Controllers;

use App\Profile;
use Illuminate\Http\Request;

class UsersProfile extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $userData = Profile::find(auth()->user()->id);


        return view('profile', compact('userData'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {

        $data = Profile::find(auth()->user()->id);
        $data->adress  = $request->adress;
        $data->adress_number = $request->adress_number;
        $data->zip = $request->zip;
        $data->phone = $request->phone;
        $data->mobile = $request->mobile;
        $data->company_name = $request->company_name;
        $data->site = $request->site;
        $data->save();


        $userData = Profile::find(auth()->user()->id);
        return view('profile', compact('userData'));


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Profile $profile)
    {

        $userData = Profile::find(auth()->user()->id);


        return view('profile', compact('userData'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function edit(Profile $profile)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Profile $profile)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function destroy(Profile $profile)
    {
        //
    }
}
